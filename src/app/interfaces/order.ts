import {Customer} from './customer';
import {Product} from './product';

export interface Order {
  id?:string;
  // customer:Customer;
  // product:Product;
  customerId: string,
  productId: string,
  staticIpAddress?:string;
  active:boolean;
}
