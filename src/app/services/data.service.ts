import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Customer} from '../interfaces/customer';
import {Product} from '../interfaces/product';
import {Ip} from '../interfaces/ip';
import {catchError, retry} from 'rxjs/operators';
import {Order} from '../interfaces/order';
import {Observable} from 'rxjs';


import * as faker from 'faker';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // private REST_API_SERVER = 'http://localhost:5000';
  // private REST_API_SERVER = 'http://142.93.175.214:8080';
  private CUSTOMER_REST_API_SERVER = 'http://142.93.175.214:2020';
  private CRM_REST_API_SERVER = 'http://142.93.175.214:4040';

  constructor(private httpClient: HttpClient) {}
  // public sendGetRequest() {
  //   return this.httpClient.get(this.REST_API_SERVER);
  // }
  public getCustomerList(errorHandlerCallback):Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.CUSTOMER_REST_API_SERVER + '/customers')
    // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
    .pipe(
        catchError(errorHandlerCallback('getCustomerList', ""))
      );
  }

  public getCustomer(id:string,errorHandlerCallback):Observable<Customer> {
    return this.httpClient.get<Customer>(this.CUSTOMER_REST_API_SERVER + '/customers/'+id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback('getCustomer', ""))
      );
  }

  public postNewCustomerData (customer: Customer, errorHandlerCallback): Observable<Customer> {
    console.log(customer);
    return this.httpClient.post<Customer>(this.CUSTOMER_REST_API_SERVER + '/customers', customer)
    // return this.httpClient.post<Customer>(this.REST_API_SERVER + '/customer', customer)
      .pipe(
        catchError(errorHandlerCallback('postNewCustomerData', customer))
      );
  }

  public updateCustomerData (customer: Customer, errorHandlerCallback): Observable<Customer> {
    // TODO: backend should do this, update both customer and order data for static ip address
    return this.httpClient.put<Customer>(this.CUSTOMER_REST_API_SERVER + '/customers/'+customer.id.toString(), customer).pipe(
      catchError(errorHandlerCallback('updateCustomerData', customer))
    );

  }

  public getProductList(errorHandlerCallback) {
    return this.httpClient.get<Product[]>(this.CRM_REST_API_SERVER + '/products');
  }

  public getProduct(id:string,errorHandlerCallback):Observable<Product> {
    return this.httpClient.get<Product>(this.CRM_REST_API_SERVER + '/products/'+id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback('getProduct', ""))
      );
  }
  // TODO: Add response error checking
  public postOrderData (order: Order, errorHandlerCallback): Observable<Order> {
    return this.httpClient.post<Order>(this.CRM_REST_API_SERVER + '/orders', order)
      .pipe(
        catchError(errorHandlerCallback('postOrderData', order))
      );
  }
  public updateOrderData (order: Order, errorHandlerCallback): Observable<Order> {
    // // TODO: backend should do this, update both customer and order data for static ip address
    // this.httpClient.put<Customer>(this.CUSTOMER_REST_API_SERVER + '/customers/'+order.customerId.toString(), order.customer).subscribe(
    //   data=>{console.log('success',data)},
    //   error => {console.log('error',error)}
    // )

    // return this.httpClient.put<Order>(this.CRM_REST_API_SERVER + '/orders/'+order.id.toString(), order).pipe(
    //   catchError(errorHandlerCallback('updateOrderData', order))
    // );

    console.log(order);
    return this.httpClient.post<Order>(this.CRM_REST_API_SERVER + '/orders/'+order.id.toString(), order).pipe(
        catchError(errorHandlerCallback('updateOrderData', order))
      );
  }

  public getOrdersByStaticIpAddress(ip:string,errorHandlerCallback){
    let params = new HttpParams().set('staticIpAddress',ip);
    return this.httpClient.get<Order[]>(this.CRM_REST_API_SERVER + '/orders', {params:params});
  }

  public getOrdersByCustomerId(id:string,errorHandlerCallback){
    let params = new HttpParams().set('customerId',id);
    return this.httpClient.get<Order[]>(this.CRM_REST_API_SERVER + '/orders', {params:params});
  }

  public getCustomerHistory(id:string,errorHandlerCallback):Observable<any> {
    return this.httpClient.get<any>(this.CUSTOMER_REST_API_SERVER + '/customers/history/'+id)
      // return this.httpClient.get<Customer[]>(this.REST_API_SERVER + '/customer');
      .pipe(
        catchError(errorHandlerCallback('getCustomerHistory', ""))
      );
  }


  // TODO: make this function observable
  public getStaticIP(errorHandlerCallback):Observable<Ip>{
    // let staticIP:string=faker.internet.ip();
    // return staticIP;
    // /network/generate-ip
    return this.httpClient.get<string>(this.CRM_REST_API_SERVER + '/network/generate-ip')
      .pipe(
        catchError(errorHandlerCallback('getStaticIP', ""))
      );
  }
}
