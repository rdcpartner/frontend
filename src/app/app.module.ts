import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './screens/home/home.component';
import { OrderDetailsComponent } from './screens/order-details/order-details.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatFormFieldModule,
  MatDialogModule, MatDividerModule
} from '@angular/material';
import {MatStepperModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {DialogOrderCompleteDialog, OrderComponent} from './screens/order/order.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MicroservicesComponent } from './screens/microservices/microservices.component';
import { MicroservicesFilterComponent } from './screens/microservices-filter/microservices-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OrderDetailsComponent,
    OrderComponent,
    DialogOrderCompleteDialog,
    MicroservicesComponent,
    MicroservicesFilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
