import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './screens/home/home.component';
import {OrderDetailsComponent} from './screens/order-details/order-details.component';
import {DialogOrderCompleteDialog, OrderComponent} from './screens/order/order.component';
import {MicroservicesComponent} from './screens/microservices/microservices.component';
import {MicroservicesFilterComponent} from './screens/microservices-filter/microservices-filter.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'order', component: OrderComponent},
  {path: 'order-details', component: OrderDetailsComponent},
  {path: 'order-details/:ip', component: OrderDetailsComponent},
  {path: 'microservices',component:MicroservicesComponent},
  {path: 'microservices/:id',component:MicroservicesComponent},
  {path: 'microservices-filter',component:MicroservicesFilterComponent},
  {path: 'microservices-filter/:id',component:MicroservicesFilterComponent},
  {path: '', component: DialogOrderCompleteDialog},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
