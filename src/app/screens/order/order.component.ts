import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../../services/data.service';
import {Data, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {Customer} from '../../interfaces/customer';
import {Product} from '../../interfaces/product';
import {throwError} from 'rxjs';
import {Order} from '../../interfaces/order';
import {MatStepper} from '@angular/material';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Ip} from '../../interfaces/ip';



@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  private isLinear = true;
  private newCustomerFormGroup: FormGroup;
  private selectCustomerFormGroup: FormGroup;
  private selectProductFormGroup: FormGroup;
  private makeOrderFormGroup: FormGroup;
  private finaliseOrderFormGroup: FormGroup;
  private customers : Array<Customer>=[];
  private products : Array<Product>=[];
  private selectedCustomer: Customer;
  private selectedProduct: Product;
  private order: Order;
  private staticIPAddress: string;
  // isActivated: boolean;
  private newCustomerFlag: boolean;
  private newCustomerFirstName: string;
  private newCustomerLastName: string;

  @ViewChild('stepper', {static:false}) stepper:MatStepper;
  constructor(public dialog: MatDialog, private router: Router, private dataService: DataService, private formBuilder: FormBuilder) { }
  ngOnInit() {
    this.getCustomerList();
    this.selectCustomerFormGroup = this.formBuilder.group({
      //TODO: Add conditional validation
        // customerSelectFormCtrl: ['', Validators.required],
        // newCustomerFirstNameFormCtrl: ['', Validators.required],
        // newCustomerLastNameFormCtrl: ['', Validators.required],
      }
      );
    this.selectProductFormGroup = this.formBuilder.group({
      productFormCtrl: ['', Validators.required]}
    );
    this.makeOrderFormGroup = this.formBuilder.group({
      // thirdCtrl: ['', Validators.required]
    });

    this.finaliseOrderFormGroup = this.formBuilder.group({
      fourthCtrl: ['', Validators.required]}
    );
  }

  // TODO: Add checking goprevious step to make sure cannot go back after an order is made
  stepperChange(event: any) {
    switch (event.selectedIndex) {
      case 0:
        // Reload our customer data
        console.log('Customer Selection');
        this.getCustomerList();
        break;
      case 1:
        console.log('Product Selection');
        this.getProductList();
        break;
      case 2:
        console.log(this.selectedCustomer);
        console.log(this.selectedProduct);
        break;
      case 3:
        this.getStaticIPAddress();
      default:
        console.log('Error on stepper');
    }
  }
  getSelectedCustomerFullName(){
    // TODO: Error handling alerts
    let customer:Customer=this.selectedCustomer;
    let customerFullName="";
    try{
      customerFullName = customer.firstName + " " + customer.lastName;
    }
    catch (e) {
    }
    return customerFullName;
  }

  onClickNewCustomerFlagButton(){
    this.newCustomerFlag=!this.newCustomerFlag;
    // Reload customer list
    this.getCustomerList();
  }

  newCustomer(){
    let newCustomer:Customer={
      firstName: this.newCustomerFirstName,
      lastName: this.newCustomerLastName,
    }
    this.dataService.postNewCustomerData(newCustomer,this.handleAPIHTTPErrors).subscribe((data: Customer) => {
      console.log(data);
      // API returns saved order data. To be on the safe side use this for the next operations, refresh the order data
      this.selectedCustomer=data;
      this.stepper.next();
    });
  }

  getCustomerList(){
    this.dataService.getCustomerList(this.handleAPIHTTPErrors).subscribe((data: Array<Customer>) => {
      // console.log(data);
      this.customers = data;
    });
  }

  getProductList(){
    this.dataService.getProductList(this.handleAPIHTTPErrors).subscribe((data: Array<Product>) => {
      // console.log(data);
      this.products = data;
    });
  }
  saveOrder(customer:Customer,product:Product){
  // saveOrder(customer:Customer,product:Product){
    this.order={
      // staticIpAddress:"",
      active:false,
      customerId:customer.id,
      productId:product.id
    };
    this.dataService.postOrderData(this.order,this.handleAPIHTTPErrors).subscribe((data: Order) => {
      console.log(data);
      // API returns saved order data. To be on the safe side use this for the next operations, refresh the order data
      this.order=data;
      this.stepper.next();
    });
  }

  finaliseOrder(){
    this.order.staticIpAddress=this.staticIPAddress;
    // this.order.customer.staticIpAddress=this.staticIPAddress;
    this.order.active=true;
    // TODO If activate selected call activate
    // Data Network activate
    // Check Response. If success save data
    // if(this.order.isActivated){
    //  console.log('Needs activation');
    // }
    console.log(this.order);
    this.dataService.updateOrderData(this.order,this.handleAPIHTTPErrors).subscribe((data: Order) => {
      console.log(data);

      const dialogRef = this.dialog.open(DialogOrderCompleteDialog, {
        width: '250px',
        data: {titleText:"Sipariş Tamamlandı",staticIpAddress: this.order.staticIpAddress}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.router.navigate(['/order-details/'+this.order.staticIpAddress]);
      });
    });

  }


  getStaticIPAddress(){
    this.dataService.getStaticIP(this.handleAPIHTTPErrors).subscribe((data:Ip)=>{
      this.staticIPAddress=data.ipv4;
      }
    )
  }

  handleAPIHTTPErrors(error: HttpErrorResponse){
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log("Error fetching data");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}


@Component({
  selector: 'dialog-order-complete',
  templateUrl: 'dialog-order-complete.html',
})
export class DialogOrderCompleteDialog {
  constructor(
    public dialogRef: MatDialogRef<DialogOrderCompleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data:DialogTextData,
    // @Inject(MAT_DIALOG_DATA) public staticIpAddress: string,
    // @Inject(MAT_DIALOG_DATA) public titleText: string,
    ) {}

  onClick(): void {
    this.dialogRef.close();
  }

}

export interface DialogTextData {
  titleText: string;
  staticIpAddress: string;
}
