import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroservicesFilterComponent } from './microservices-filter.component';

describe('MicroservicesFilterComponent', () => {
  let component: MicroservicesFilterComponent;
  let fixture: ComponentFixture<MicroservicesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MicroservicesFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroservicesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
