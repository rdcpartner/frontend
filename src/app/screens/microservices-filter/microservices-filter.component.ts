import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Customer} from '../../interfaces/customer';
import {Order} from '../../interfaces/order';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {DataService} from '../../services/data.service';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {Product} from '../../interfaces/product';

@Component({
  selector: 'app-microservices-filter',
  templateUrl: './microservices-filter.component.html',
  styleUrls: ['./microservices-filter.component.scss']
})
export class MicroservicesFilterComponent implements OnInit {
  private selectCustomerFormGroup: FormGroup;
  private customers : Array<Customer>=[];
  private selectedCustomer: Customer;
  private customerId:string;

  // private order:Order;
  constructor(private route:ActivatedRoute, public dialog: MatDialog, private router: Router, private dataService: DataService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.customerId = this.route.snapshot.queryParamMap.get('id');
    this.getCustomerList();
    this.selectCustomerFormGroup = this.formBuilder.group({
        customerSelectFormCtrl: ['', Validators.required],
      }
    );
    this.selectCustomerFormGroup.get('customerSelectFormCtrl').valueChanges.subscribe(value=>{
      try{
        let customer:Customer=value;
        this.getCustomerHistory(customer.id);
      }
      catch (e) {

      }
    });
  }



  getCustomerHistory(id:string){
    this.dataService.getCustomerHistory(id,this.handleAPIHTTPErrors).subscribe((data: any) => {
        console.log(data);
        }
    );
  }

  // TODO: Move to another service
  getCustomerList(){
    this.dataService.getCustomerList(this.handleAPIHTTPErrors).subscribe((data: Array<Customer>) => {
      // console.log(data);
      this.customers = data;
    });
  }

  handleAPIHTTPErrors(error: HttpErrorResponse){
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log("Error fetching data");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
