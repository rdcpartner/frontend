import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {Customer} from '../../interfaces/customer';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {Order} from '../../interfaces/order';
import {Product} from '../../interfaces/product';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  private selectCustomerFormGroup: FormGroup;
  private customers : Array<Customer>=[];
  private selectedCustomer: Customer;
  private staticIpAddress:string;
  private orders: Array<Order>=[];
  private ordersProductData: Array<OrderData>=[]; //Map<string,Order>;
  // private order:Order;
  constructor(private route:ActivatedRoute, public dialog: MatDialog, private router: Router, private dataService: DataService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.staticIpAddress = this.route.snapshot.queryParamMap.get('ip');
    this.getCustomerList();
    this.selectCustomerFormGroup = this.formBuilder.group({
        customerSelectFormCtrl: ['', Validators.required],
      }
    );
    this.selectCustomerFormGroup.get('customerSelectFormCtrl').valueChanges.subscribe(value=>{
      // this.order=null;
      this.orders=[];
      try{
        let customer:Customer=value;
        this.getOrdersByCustomerId(customer.id);
      }
      catch (e) {

      }
    });
  }

  getOrdersByCustomerId(id:string){
    this.dataService.getOrdersByCustomerId(id,this.handleAPIHTTPErrors).subscribe((data: Array<Order>) => {
        console.log(data);
        console.log(data.toString());
        this.orders = data;

        this.ordersProductData = [];
        for (let i = 0; i < this.orders.length; i++) {
          let order = <Order> this.orders[i];
          console.log(order);
          // Check if customer is the selected customer
          if(order.customerId===id){
            this.ordersProductData=[];
            this.dataService.getProduct(order.productId, this.handleAPIHTTPErrors).subscribe((data: Product) => {
              // console.log(data);
              let orderData:OrderData={
                active: order.active,
                id: order.id,
                productName: data.name,
                staticIpAddress: order.staticIpAddress
              };
              // console.log(orderData);
              this.ordersProductData.push(orderData);
            });
          }
        }
      }
      // if(this.orders.length>0){
      //   this.order=this.orders[0];
      // }
    );
  }

  getOrderProductData(i:number):OrderData{
    // console.log(this.ordersProductData);
   return this.ordersProductData[i];
  }

  getOrderByStaticIpAddress(ip:string){
    this.dataService.getOrdersByStaticIpAddress(ip,this.handleAPIHTTPErrors).subscribe((data: Array<Order>) => {
        // console.log(data);
        // console.log(data.toString());
        this.orders = data;

        this.ordersProductData = [];
        for (let i = 0; i < this.orders.length; i++) {
          let order = <Order> this.orders[i];
          this.dataService.getProduct(order.productId, this.handleAPIHTTPErrors).subscribe((data: Product) => {
            // console.log(data);
            let orderData:OrderData={
              active: order.active,
              id: order.id,
              productName: data.name,
              staticIpAddress: order.staticIpAddress
            };
            // console.log(orderData);
            this.ordersProductData.push(orderData);
            //this.ordersProductData.push(data);
          });
        }
      }
      // if(this.orders.length>0){
      //   this.order=this.orders[0];
      // }
    );
  }

  // TODO: Move to another service
  getCustomerList(){
    this.dataService.getCustomerList(this.handleAPIHTTPErrors).subscribe((data: Array<Customer>) => {
      // console.log(data);
      this.customers = data;
    });
  }

  handleAPIHTTPErrors(error: HttpErrorResponse){
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log("Error fetching data");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}

// Temporary interface no need to add it to app wide interfaces
export interface OrderData {
  id:string;
  productName: string,
  staticIpAddress:string;
  active:boolean;
}
