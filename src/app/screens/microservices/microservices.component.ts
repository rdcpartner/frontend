import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {Customer} from '../../interfaces/customer';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-microservices',
  templateUrl: './microservices.component.html',
  styleUrls: ['./microservices.component.scss']
})
export class MicroservicesComponent implements OnInit {
  private selectCustomerFormGroup: FormGroup;
  private editCustomerFormGroup: FormGroup;
  private users : Array<Customer>=[];
  private newCustomerFlag: boolean;
  private newCustomerFirstName: string;
  private newCustomerLastName: string;
  private newCustomerEmail: string;
  private selectedCustomer: Customer;
  private selectedCustomerFirstName: string;
  private selectedCustomerLastName: string;
  private selectedCustomerEmail: string;
  private userId:string;
  constructor(public dialog: MatDialog, private route:ActivatedRoute, private router: Router, private dataService: DataService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getCustomerList();
    // this.userId = this.route.snapshot.queryParamMap.get('id');
    this.route.paramMap.subscribe(
      params=>{
        this.userId=params.get('id');
        if(this.userId){
          console.log(this.userId);
          this.dataService.getCustomer(this.userId,this.handleAPIHTTPErrors).subscribe((data: Customer) => {
            console.log(data);
            this.selectedCustomer=data;
            this.selectedCustomerFirstName=this.selectedCustomer.firstName;
            this.selectedCustomerLastName=this.selectedCustomer.lastName;
            // this.selectedCustomerEmail=this.selectedCustomer.email;
          });
        }
      }
    )



    this.selectCustomerFormGroup = this.formBuilder.group({
        //TODO: Add conditional validation
        userSelectFormCtrl: ['',Validators.requiredTrue],
        newCustomerFirstNameFormCtrl: ['', Validators.required],
        newCustomerLastNameFormCtrl: ['', Validators.required],
        // newCustomerEmailFormCtrl: ['', Validators.required],
      }
    );

    this.editCustomerFormGroup = this.formBuilder.group({
        editCustomerFirstNameFormCtrl: ['', Validators.required],
        editCustomerLastNameFormCtrl: ['', Validators.required],
        // editCustomerEmailFormCtrl: ['', Validators.required],
      }
    );


    this.selectCustomerFormGroup.get('userSelectFormCtrl').valueChanges.subscribe(value=>{
      console.log(value)
      if(value){
        this.selectedCustomer=value;
        this.selectedCustomerFirstName=this.selectedCustomer.firstName;
        this.selectedCustomerLastName=this.selectedCustomer.lastName;
        // this.selectedCustomerEmail=this.selectedCustomer.email
      }
    });

  }

  onClickNewCustomerFlagButton(){
    this.newCustomerFlag=!this.newCustomerFlag;
    // Reload customer list
    this.getCustomerList();
  }
  newCustomer(){
    let newCustomer:Customer={
      firstName: this.newCustomerFirstName,
      lastName: this.newCustomerLastName,
    }
    this.dataService.postNewCustomerData(newCustomer,this.handleAPIHTTPErrors).subscribe((data: Customer) => {
      console.log(data);
      this.selectedCustomer=data;
    });
  }

  saveCustomerData(){
    this.selectedCustomer.firstName=this.selectedCustomerFirstName;
    this.selectedCustomer.lastName=this.selectedCustomerLastName;
    // this.selectedCustomer.email=this.selectedCustomerEmail;
    this.dataService.updateCustomerData(this.selectedCustomer,this.handleAPIHTTPErrors).subscribe((data: Customer) => {
      console.log(data);
      this.selectedCustomer=data;
    });
    // this.selectCustomerFormGroup.get('userSelectFormCtrl').setValue(this.selectedCustomer);
  }

  getCustomerList(){
    this.dataService.getCustomerList(this.handleAPIHTTPErrors).subscribe((data: Array<Customer>) => {
      // console.log(data);
      this.users = data;
    });
  }

  handleAPIHTTPErrors(error: HttpErrorResponse){
    // TODO: Parse error message and Show alert to user
    // TODO: Make error handling app wide instead of component based
    console.log("Error fetching data");
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

}
