let faker=require('faker');

let database = {  customers:[], products: [], orders:[], logs:[]};

for (let i=0;i<9;i++){
  database.products.push({
    id: faker.random.uuid().replace(/-/g,10).substr(0,32),
    name: faker.commerce.productName(),
    description: faker.lorem.sentences()
  })
}


for (let i=0; i<20;i++){
  database.customers.push({
    id: faker.random.uuid().replace(/-/g,10).substr(0,32),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    staticIpAddress: ''
  })
}

for (let i=0; i<20;i++){
  database.logs.push({
    id: faker.random.uuid().replace(/-/g,10).substr(0,32),
    firstNameNew: faker.name.firstName(),
    firstNameOld: faker.name.firstName(),
    lastNameNew: faker.name.lastName(),
    lastNameOld: faker.name.lastName(),
    staticIpAddressOld: faker.internet.ip(),
    staticIpAddressNew: faker.internet.ip()
  })
}


console.log(JSON.stringify(database, null, 4));
